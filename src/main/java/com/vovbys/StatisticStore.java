package com.vovbys;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StatisticStore {
    private static Map<String, Map<String,String>> store = new HashMap<>();

    static Map<String, Map<String,String>> getStore() {
        return store;
    }

    static void createNewKeys(String[] currentHeader) {
        for (String header:  currentHeader) {
            if(!store.containsKey(header)){
                store.put(header, new ConcurrentHashMap<>());
            }
        }
    }

    public static void print(){
        store.forEach(StatisticStore::printRow);
    }

    private static void printRow(String head, Map<String, String> map) {
        System.out.println(head+":");
        map.forEach((k,v)-> System.out.print(k+";"));
        System.out.println();
    }
}
