package com.vovbys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Objects;

public class DataReader {
    private DataManager dataManager = new DataManager();

    public void init(int countThread,InputStream in) throws IOException, InterruptedException {
        dataManager.start(countThread);
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String[] currentHeader = null;

        while (true) {
            String str = reader.readLine();

            if (str == null || str.equals("")) {
                break;
            }

            if (str.matches(".*\\.csv([;:])?")) {
                currentHeader = reader.readLine().split(";");
                StatisticStore.createNewKeys(currentHeader);

            } else {
                Objects.requireNonNull(currentHeader);
                dataManager.getQueue().add(new Element(currentHeader, str));
            }
        }

        while(!dataManager.getQueue().isEmpty()){
            Thread.sleep(1000);
        }
        dataManager.stop();
    }
}
