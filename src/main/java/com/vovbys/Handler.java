package com.vovbys;

import java.util.Map;

class Handler {
    void handle(Element element) {
        Map<String, Map<String, String>> store = StatisticStore.getStore();

        String[] header = element.getHeader();
        String[] values = element.getStr().split(";");
        for (int i = 0; i < header.length; i++) {
            store.get(header[i]).put(values[i], "");
        }
    }
}
