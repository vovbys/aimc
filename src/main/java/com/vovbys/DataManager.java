package com.vovbys;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

import static java.lang.Thread.currentThread;
import static java.util.concurrent.Executors.newFixedThreadPool;
import static java.util.concurrent.TimeUnit.HOURS;

class DataManager {
    private ExecutorService executorService;
    private final BlockingQueue<Element> queue = new LinkedBlockingQueue<>();
    private final Handler handler = new Handler();

    void start(int countThread) {
        executorService = newFixedThreadPool(countThread);
        for (int i = 0; i < countThread; i++) {
            executorService.execute(this::taskWrapper);
        }
    }

    void stop() throws InterruptedException {
        executorService.shutdownNow();
        executorService.awaitTermination(1, HOURS);
    }

    BlockingQueue<Element> getQueue() {
        return queue;
    }

    private void taskWrapper() {
        try {
            while (!currentThread().isInterrupted()) {
                handler.handle(queue.take());
            }
        } catch (InterruptedException e) {
            currentThread().interrupt();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
