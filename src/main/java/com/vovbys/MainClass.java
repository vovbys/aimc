package com.vovbys;

import java.io.IOException;

public class MainClass {
    public static void main(String[] args) throws IOException, InterruptedException {
        new DataReader().init(4,System.in);
        StatisticStore.print();
    }
}
