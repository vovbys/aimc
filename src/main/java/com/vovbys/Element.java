package com.vovbys;

class Element {
    private String[] header;
    private String row;

    Element(String[] header, String row) {
        this.header = header;
        this.row = row;
    }

    String getStr() {
        return row;
    }

    String[] getHeader() {
        return header;
    }
}
