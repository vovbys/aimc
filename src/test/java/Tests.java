
import com.vovbys.DataReader;
import com.vovbys.StatisticStore;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;

public class Tests {
    private File smallData = new File("src/test/resources/input/smallData.txt");
    private File bigData = new File("src/test/resources/input/bigData.txt");
    private File bigData2 = new File("src/test/resources/input/bigData2.txt");

    @Test
    @Ignore
    public void simpleTest() throws IOException, InterruptedException {
        InputStream inputStream = new FileInputStream(smallData);
        new DataReader().init(1, inputStream);
        StatisticStore.print();
    }

    @Test
    @Ignore
    public void bigTestSingleCore() throws IOException, InterruptedException {
        InputStream inputStream = new FileInputStream(bigData2);
        new DataReader().init(1, inputStream);
    }

    @Test
    @Ignore
    public void bigTestMultiCore() throws IOException, InterruptedException {
        InputStream inputStream = new FileInputStream(bigData);
        new DataReader().init(4, inputStream);
        StatisticStore.print();
    }

    @Test
    @Ignore
    public void createBigFile1() throws IOException {
        InputStream inputStream = new FileInputStream(smallData);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ArrayList<String> strings = new ArrayList<>();

        while (true) {
            String str = reader.readLine();

            if (str == null || str.equals("")) {
                break;
            }
            strings.add(str);
        }


        FileWriter writer = new FileWriter(bigData);

        for (int i = 0; i < 2000000; i++) {
            for (String string : strings) {
                writer.write(string + "\n");
            }
            writer.flush();
        }
        writer.close();

    }

    @Test
    @Ignore
    public void createBigFile2() throws IOException {
        FileWriter writer = new FileWriter(bigData2);
        writer.write("123.csv:" + "\n");
        writer.write("a;b;c;" + "\n");

        for (int i = 0; i < 4000000; i++) {
            writer.write(i + ";" + i % 2000 + ";" + i % 5000 + ";" + "\n");
            writer.flush();
        }
        writer.close();

    }
}
